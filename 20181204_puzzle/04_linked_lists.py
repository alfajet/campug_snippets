"""
Challenge 5 - Linked lists
http://www.pythonchallenge.com/pc/def/linkedlist.php
"""
import requests
import re

base_url = 'http://www.pythonchallenge.com/pc/def/linkedlist.php?nothing='
seed = 12345


def get_next_page(number):
    """
    Recursive method, querying "linkedlist" page with the provided number unless other instructions are provided.
    :param number: initial number to be queried
    :return: if it doesn't recurse, the requested page's text will be printed
    """
    pattern = r'.*and the next nothing is (\d+)'
    r = requests.get(base_url + str(number))

    if r.status_code == requests.codes.ok:
        match = re.match(pattern, r.text)
        if match:
            next_number = match.group(1)
            print(next_number)
            get_next_page(next_number)
        else:
            print(r.text)


get_next_page(63579)


