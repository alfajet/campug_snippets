"""
Challenge 06 - Peak Hell
http://www.pythonchallenge.com/pc/def/peak.html
"""
import requests
import pickle
from io import BytesIO


r = requests.get('http://www.pythonchallenge.com/pc/def/banner.p')
if r.status_code == requests.codes.ok:
    b = BytesIO(r.content)
    data = pickle.load(b)
    for line in data:
        print(''.join([i * j for (i, j) in line]))
