import requests
import PIL.Image
from PIL.ExifTags import TAGS
from io import BytesIO

r = requests.get('http://www.pythonchallenge.com/pc/def/channel.jpg')
if r.status_code == requests.codes.ok:
    photo_stream = BytesIO(r.content)
    photo = PIL.Image.open(photo_stream)
    exif_tags = photo._getexif()
    print(exif_tags)
    print(photo.info)