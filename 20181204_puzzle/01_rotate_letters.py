"""
Challenge 02 - Map
http://www.pythonchallenge.com/pc/def/map.html
"""

ORD_a = 97  # ascii code for a
ORD_z = 122  # ascii code for z


def rotate(input_str, rotation):
    """
    Apply rotation to alphabetical characters only. Others such as space are left as is.

    :param input_str: string to be decrypted
    :type input_str: str

    :param rotation: number of characters to be rotated (e.g. 1 a => b)
    :type rotation: int

    :return: decrypted string
    :rtype: str
    """
    new_string = []  # Decrypted letters are stored in a list
    for char in input_str:
        if ord(char) in range(ORD_a, ORD_z + 1):  # We rotate only letters, other characters are left as they are.
            char_value = ord(char) - ORD_a
            new_char_value = (char_value + rotation) % 26
            new_char = chr(new_char_value + 97)
        else:
            new_char = char
        new_string.append(new_char)
    return ''.join(new_string)  # Concatenating the list in a string before returning the result


encrypted_string = """g fmnc wms bgblr rpylqjyrc gr zw fylb. rfyrq ufyr amknsrcpq ypc dmp. 
bmgle gr gl zw fylb gq glcddgagclr ylb rfyr'q ufw rfgq rcvr gq qm jmle. 
sqgle qrpgle.kyicrpylq() gq pcamkkclbcb. lmu ynnjw ml rfc spj. """

print(rotate(encrypted_string, 2))
"""
Output:
i hope you didnt translate it by hand. thats what computers are for. 
doing it in by hand is inefficient and that's why this text is so long. 
using string.maketrans() is recommended. now apply on the url. 
"""

print(rotate('map', 2))
"""
Output:
ocr
"""
