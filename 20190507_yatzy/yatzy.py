class Yatzy:

    def __init__(self, d1, d2, d3, d4, d5):
        self.dice = [0]*5
        self.dice[0] = d1
        self.dice[1] = d2
        self.dice[2] = d3
        self.dice[3] = d4
        self.dice[4] = d5

        self.dice_freq = [0] * 7
        self.calc_frequency()

    def calc_frequency(self):
        self.dice_freq = [0] * 7
        for die in self.dice:
            self.dice_freq[die] += 1

    def chance(self):
        total = sum(self.dice)
        return total

    def yatzy(self):
        try:
            self.dice_freq.index(5)
            return 50
        except ValueError:
            return 0

    def ones(self):
        return self.dice_freq[1] * 1

    def twos(self):
        return self.dice_freq[2] * 2

    def threes(self):
        return self.dice_freq[3] * 3

    def fours(self):
        return self.dice_freq[4] * 4

    def fives(self):
        return self.dice_freq[5] * 5

    def sixes(self):
        return self.dice_freq[6] * 6

    def score_pair(self):
        return self.score_x_of_kind(2, 1)

    def two_pair(self):
        return self.score_x_of_kind(2, 2)

    def four_of_a_kind(self):
        return self.score_x_of_kind(4, 1)

    def three_of_a_kind(self):
        return self.score_x_of_kind(3, 1)

    def score_x_of_kind(self, kinds, nb_kind):
        total_kinds = 0
        score = 0
        for i in range(6, 0, -1):
            kinds_current_nb = int(self.dice_freq[i] / kinds)
            if kinds_current_nb < (nb_kind - total_kinds):
                total_kinds += kinds_current_nb
                score += kinds_current_nb * i * kinds
            else:
                score += (nb_kind - total_kinds) * i * kinds
                total_kinds = nb_kind
                break

        if total_kinds == nb_kind:
            return score
        else:
            return 0

    def _straight(self, start, stop, score):
        for i in range(start, stop + 1):
            if self.dice_freq[i] != 1:
                return 0
        return score

    def small_straight(self):
        return self._straight(1, 5, 15)

    def large_straight(self):
        return self._straight(2, 6, 20)

    def full_house(self):
        try:
            three_kind = self.dice_freq.index(3)
            two_kind = self.dice_freq.index(2)
            return three_kind * 3 + two_kind * 2
        except ValueError:
            return 0
